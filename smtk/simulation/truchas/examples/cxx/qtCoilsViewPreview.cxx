//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/attribute/Resource.h"
#include "smtk/extension/qt/qtBaseView.h"
#include "smtk/extension/qt/qtUIManager.h"
#include "smtk/io/AttributeReader.h"
#include "smtk/io/AttributeWriter.h"
#include "smtk/io/Logger.h"
#include "smtk/view/View.h"

#include "smtk/simulation/truchas/plugin/smtkTruchasCoilsView.h"

#include <QApplication>
#include <QVBoxLayout>
#include <QWidget>

#include <cassert>
#include <iostream>

int main(int argc, char* argv[])
{
  if (argc < 2)
  {
    std::cout << "\n"
              << "Load attribute resource and display induction-heating attribute\n"
              << "with custom view of the coils model data."
              << "\n"
              << "Usage: qtCoilsModelPreview attribute_filename  [output_filename]"
              << "\n"
              << std::endl;
    return -1;
  }

  QApplication app(argc, argv);

  // Instantiate and load attribute resource
  smtk::attribute::ResourcePtr attResource = smtk::attribute::Resource::create();
  char* inputPath = argv[1];
  smtk::io::AttributeReader reader;
  smtk::io::Logger inputLogger;
  bool err = reader.read(attResource, inputPath, true, inputLogger);
  if (err)
  {
    std::cout << "Error loading attribute file -- exiting"
              << "\n";
    std::cout << inputLogger.convertToString() << std::endl;
    return -2;
  }

  // Instantiate smtk's qtUIManager and register coils view
  smtk::extension::qtUIManager* uiManager = new smtk::extension::qtUIManager(attResource);
  uiManager->registerViewConstructor(
    "smtkTruchasCoilsView", smtkTruchasCoilsView::createViewWidget);

  // Instantiate empty widget as containter for qtUIManager
  QWidget* widget = new QWidget();
  QVBoxLayout* layout = new QVBoxLayout();
  widget->setLayout(layout);

  // Get view and display widget
  smtk::view::ViewPtr view = attResource->findTopLevelView();
  uiManager->setSMTKView(view, widget);
  widget->resize(800, 600);
  widget->show();

  // Run the event loop
  int retval = app.exec();
  QCoreApplication::processEvents();

  if (argc > 2)
  {
    char* outputPath = argv[2];
    std::cout << "Writing resulting simulation file: " << outputPath << std::endl;
    smtk::io::AttributeWriter writer;
    smtk::io::Logger outputLogger;
    bool outputErr = writer.write(attResource, outputPath, outputLogger);
    if (outputErr)
    {
      std::cout << "Error writing simulation file -- exiting"
                << "\n"
                << outputLogger.convertToString() << std::endl;
      return -3;
    }
  }

  // Release resources

  return retval;
}
