//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
// .NAME smtkTruchasCoilsView.h - SMTK view for induction heating coils

#ifndef __smtk_simulation_truchas_qt_smtkTruchasCoilsView_h
#define __smtk_simulation_truchas_qt_smtkTruchasCoilsView_h

#include "smtk/PublicPointerDefs.h"
#include "smtk/extension/qt/qtBaseView.h"

#include "smtk/simulation/truchas/qt/Exports.h"

#include <QItemSelection>

class smtkTruchasCoilsViewInternals;

/* **
  Provides a widget encapsulating the custom editors for Truchas
  induction heating sources and coils. Takes as input a
  "induction-heating" attribute with group items for coils and
  source data.
 */

typedef smtk::extension::qtBaseView qtBaseView;
typedef smtk::extension::ViewInfo ViewInfo;

class SMTKTRUCHASQTEXT_EXPORT smtkTruchasCoilsView : public qtBaseView
{
  Q_OBJECT

public:
  static qtBaseView* createViewWidget(const ViewInfo& info);
  smtkTruchasCoilsView(const ViewInfo& info);
  virtual ~smtkTruchasCoilsView();

  bool isEmpty() const override;

protected:
  void createWidget() override;

private:
  smtkTruchasCoilsViewInternals* Internals;
};

#endif // __smtk_simulation_truchas_qt_smtkTruchasCoilsView_h
