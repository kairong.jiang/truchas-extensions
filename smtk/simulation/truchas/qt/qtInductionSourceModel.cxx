//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/simulation/truchas/qt/qtInductionSourceModel.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ValueItem.h"

#include <QBrush>
#include <QColor>
#include <QDebug>
#include <QMimeData>
#include <QModelIndex>
#include <QString>
#include <QTextStream>
#include <QVariant>
#include <QWidget>

#include <cassert>

qtInductionSourceModel::qtInductionSourceModel(
  QWidget* parent, smtk::attribute::AttributePtr attribute)
  : QAbstractTableModel(parent)
  , m_attribute(attribute)
  , m_sourceItem(nullptr)
  , m_coilsItem(nullptr)
{
  m_sourceItem = attribute->findGroup("source");
  assert(m_sourceItem);
  m_coilsItem = attribute->findGroup("coils");
  assert(m_coilsItem);
}

qtInductionSourceModel::~qtInductionSourceModel()
{
}

int qtInductionSourceModel::rowCount(const QModelIndex& parent) const
{
  if (parent.isValid())
  {
    return 0;
  }
  return m_sourceItem->numberOfGroups();
}

int qtInductionSourceModel::columnCount(const QModelIndex& parent) const
{
  if (parent.isValid())
  {
    return 0;
  }
  // Two fixed columns for time & frequency
  // Plus one column for each coil (electrical current value)
  return 2 + m_coilsItem->numberOfGroups();
}

QVariant qtInductionSourceModel::data(const QModelIndex& index, int role) const
{
  QVariant qValue; // return value
  if (!index.isValid())
  {
    return qValue;
  }
  assert(index.model() == this);

  // Right-align all data
  if (role == Qt::TextAlignmentRole)
  {
    return QVariant(Qt::AlignRight | Qt::AlignVCenter);
  }

  int row = index.row();
  int col = index.column();

  if (role == Qt::BackgroundRole)
  {
    // Use default for first row & column, which is always disabled.
    if ((0 == row) && (0 == col))
    {
      return qValue;
    }

    // Get ValueItem corresponding to row, col
    smtk::attribute::ValueItemPtr valItem;
    std::size_t valIndex = 0;
    if (0 == col)
    {
      valItem = m_sourceItem->findAs<smtk::attribute::ValueItem>(row, "time");
    }
    else if (1 == col)
    {
      valItem = m_sourceItem->findAs<smtk::attribute::ValueItem>(row, "frequency");
    }
    else
    {
      // Columns 2 and higher are coil electrical-current values
      valItem = m_coilsItem->findAs<smtk::attribute::ValueItem>(col - 2, "current");
      valIndex = row;
    }

    // Find the "set" condition for the corresponding value item
    if (!valItem->isSet(valIndex))
    {
      // Invalid color
      QColor invalidColor(255, 128, 128);
      return QVariant(QBrush(invalidColor));
    }

    // (else) default
    return qValue;
  } // if (Qt::BackgroundRole)

  // Skip everything else except display role for now
  if (role != Qt::DisplayRole && role != Qt::EditRole)
  {
    // qWarning() << "Unsupported role:" << role;
    return qValue;
  }

  // DataRole
  if (0 == col) // time
  {
    // Special case -- time (0,0) is disabled
    if (0 == row)
    {
      return QVariant("n/a");
    }
    auto timeItem = m_sourceItem->findAs<smtk::attribute::DoubleItem>(row, "time");
    if (timeItem->isSet())
    {
      auto qString = QString::number(timeItem->value());
      qValue.setValue(qString);
    }
  }
  else if (1 == col) // frequency
  {
    auto freqItem = m_sourceItem->findAs<smtk::attribute::IntItem>(row, "frequency");
    if (freqItem->isSet())
    {
      auto qString = QString::number(freqItem->value());
      qValue.setValue(qString);
    }
  }
  else // coil (electrical current)
  {
    int i = col - 2;
    auto currentItem = m_coilsItem->findAs<smtk::attribute::DoubleItem>(i, "current");
    if (currentItem->isSet(row))
    {
      auto qString = QString::number(currentItem->value(row));
      qValue.setValue(qString);
    }
  }

  return qValue;
}

Qt::ItemFlags qtInductionSourceModel::flags(const QModelIndex& index) const
{
  if (!index.isValid())
  {
    return Qt::ItemIsDropEnabled;
  }

  // Special case -- time (0,0) is disabled
  if ((index.row() == 0) && (index.column() == 0))
  {
    return QAbstractTableModel::flags(index) & ~Qt::ItemIsEnabled;
  }

  auto f = Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsDragEnabled |
    Qt::ItemIsDropEnabled;
  return f;
}

QVariant qtInductionSourceModel::headerData(
  int section, Qt::Orientation orientation, int role) const
{
  if ((role != Qt::DisplayRole) || (orientation != Qt::Horizontal))
  {
    return Superclass::headerData(section, orientation, role);
  }

  if (0 == section)
  {
    return QVariant("Time");
  }
  else if (1 == section)
  {
    return QVariant("Frequency");
  }

  // (else)
  QString label;
  QTextStream qs(&label);
  qs << "Coil " << (section - 1) << "\nCurrent";
  return QVariant(label);
}

bool qtInductionSourceModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
  if (!index.isValid() || role != Qt::EditRole)
  {
    return false;
  }

  int row = index.row();
  int col = index.column();
  bool modified = false;

  // Unset the item if input is null
  if (value.toString().isEmpty())
  {
    smtk::attribute::ValueItemPtr valItem;
    std::size_t valIndex = 0;
    if (0 == col)
    {
      valItem = m_sourceItem->findAs<smtk::attribute::ValueItem>(row, "time");
    }
    else if (1 == col)
    {
      valItem = m_sourceItem->findAs<smtk::attribute::ValueItem>(row, "frequency");
    }
    else
    {
      valItem = m_coilsItem->findAs<smtk::attribute::ValueItem>(col - 2, "current");
      valIndex = row;
    }
    valItem->unset(valIndex);
    emit this->dataChanged(index, index);
    return true;
  }

  if (0 == col) // time
  {
    auto item = m_sourceItem->findAs<smtk::attribute::DoubleItem>(row, "time");
    modified = item->setValue(value.toDouble());
  }
  else if (1 == col) // frequency
  {
    auto item = m_sourceItem->findAs<smtk::attribute::IntItem>(row, "frequency");
    modified = item->setValue(value.toInt());
  }
  else // coils (electrical current)
  {
    int i = col - 2;
    auto currentItem = m_coilsItem->findAs<smtk::attribute::DoubleItem>(i, "current");
    modified = currentItem->setValue(row, value.toDouble());
  }

  if (modified)
  {
    emit this->dataChanged(index, index);
  }
  return modified;
}

bool qtInductionSourceModel::dropMimeData(const QMimeData* mimeData, Qt::DropAction action,
  int dropRow, int dropColumn, const QModelIndex& parent)
{
  int dropGroupPosition = dropRow;
  if (parent.isValid())
  {
    dropRow = parent.row();
    dropGroupPosition = dropRow;
    dropColumn = parent.column();
  }
  if (dropRow < 0)
  {
    dropRow = this->rowCount();
    dropGroupPosition = dropRow - 1;
  }

  // Parse mime data to obtain the row number that was dragged.
  QByteArray encoded = mimeData->data("application/x-qabstractitemmodeldatalist");
  QDataStream stream(&encoded, QIODevice::ReadOnly);
  QMap<int, QVariant> roleDataMap;
  int dragRow;
  stream >> dragRow;

  QModelIndex target;
  // Check that move is valid
  bool valid = this->beginMoveRows(target, dragRow, dragRow, target, dropRow);
  if (!valid)
  {
    return false;
  }

  // Rotate source item
  m_sourceItem->rotate(dragRow, dropGroupPosition);

  // Rotate current item for each coil
  for (std::size_t i = 0; i < m_coilsItem->numberOfGroups(); ++i)
  {
    auto currentItem = m_coilsItem->findAs<smtk::attribute::DoubleItem>(i, "current");
    currentItem->rotate(dragRow, dropGroupPosition);
  }

  this->endMoveRows();

  // Emit dataChanged() signal
  int firstRow = dragRow < dropRow ? dragRow : dropRow;
  int lastRow = dragRow < dropRow ? dropRow : dragRow;
  int firstCol = 0;
  int lastCol = this->columnCount() - 1;
  QModelIndex firstIndex = this->index(firstRow, firstCol);
  QModelIndex lastIndex = this->index(lastRow, lastCol);
  emit this->dataChanged(firstIndex, lastIndex);
  return true;
}

bool qtInductionSourceModel::rotateLeft(std::size_t row)
{
  QModelIndex parent;
  bool valid = this->beginMoveRows(parent, row, row, parent, row - 1);
  assert(valid);

  // Rotate source item
  m_sourceItem->rotate(row, row - 1);

  // Rotate current item for each coil
  for (std::size_t i = 0; i < m_coilsItem->numberOfGroups(); ++i)
  {
    auto currentItem = m_coilsItem->findAs<smtk::attribute::DoubleItem>(i, "current");
    currentItem->rotate(row, row - 1);
  }

  this->endMoveRows();
  return true;
}

void qtInductionSourceModel::beginAddCoil()
{
  // External logic will be adding one column to my model
  int ncols = this->columnCount();
  this->beginInsertColumns(QModelIndex(), ncols, ncols);
}

void qtInductionSourceModel::endAddCoil()
{
  this->endInsertColumns();
}

void qtInductionSourceModel::beginRemoveCoil(int row)
{
  // External logic will be removing one column to my model
  int col = row + 2;
  this->beginRemoveColumns(QModelIndex(), col, col);
}

void qtInductionSourceModel::endRemoveCoil()
{
  this->endRemoveColumns();
}



bool qtInductionSourceModel::beginRotateCoil(int fromCol, int toCol)
{
  // External logic will be rotating items in m_coilsGroup
  QModelIndex index;
  return this->beginMoveColumns(index, fromCol, fromCol, index, toCol);
}

void qtInductionSourceModel::endRotateCoil()
{
  this->endMoveColumns();
}




void qtInductionSourceModel::beginAddTime()
{
  // External logic will be adding one row to my model
  int nrows = this->rowCount();
  this->beginInsertRows(QModelIndex(), nrows, nrows);
}

void qtInductionSourceModel::endAddTime()
{
  this->endInsertRows();
}

void qtInductionSourceModel::beginRemoveTime(int row)
{
  this->beginRemoveRows(QModelIndex(), row, row);
}

void qtInductionSourceModel::endRemoveTime()
{
  this->endRemoveRows();
}
