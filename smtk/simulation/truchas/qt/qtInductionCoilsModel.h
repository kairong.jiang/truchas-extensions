//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
// .NAME qtInductionCoilsModel.h - Qt model for induction heating coils

#ifndef __smtk_simulation_truchas_qt_qtInductionCoilsModel_h
#define __smtk_simulation_truchas_qt_qtInductionCoilsModel_h

#include "smtk/PublicPointerDefs.h"

#include "smtk/simulation/truchas/qt/Exports.h"

#include <QAbstractTableModel>

class QMimeData;

/* **
  Qt table model for induction coil geometry, based on
  "induction-heating" attribute.  The table has one row for each coil, with
  colunms for number of turns, radius, length, and center.

  The input attribute contains an extensible group item named "coils".
  Each subgroup contains the items:
    * Int item "NTurns"  (number of turns)
    * Double item "radius"
    * Double item "length"
    * Double item "center" (length 3)
 */

class SMTKTRUCHASQTEXT_EXPORT qtInductionCoilsModel : public QAbstractTableModel
{
  Q_OBJECT
  typedef QAbstractTableModel Superclass;

public:
  qtInductionCoilsModel(QWidget* parent, smtk::attribute::AttributePtr attribute);
  ~qtInductionCoilsModel();

  int rowCount(const QModelIndex& parent = QModelIndex()) const override;
  int columnCount(const QModelIndex& parent = QModelIndex()) const override;
  QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
  Qt::ItemFlags flags(const QModelIndex& index) const override;
  QVariant headerData(
    int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
  bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;
  bool dropMimeData(const QMimeData* data, Qt::DropAction action, int row, int column,
    const QModelIndex& parent) override;


  // Reactions to external changes to attribute
  void beginAddCoil();
  void endAddCoil();
  void beginRemoveCoil(int row);
  void endRemoveCoil();
  bool beginRotateCoil(int fromRow, int toRow);
  void endRotateCoil();

  void beginAddTime();
  void endAddTime();
  void beginRemoveTime(int row);
  void endRemoveTime();

signals:
  void requestRotate(int fromRow, int toRow);

protected:
  smtk::attribute::AttributePtr m_attribute;
  smtk::attribute::GroupItemPtr m_coilsItem;
};

#endif
