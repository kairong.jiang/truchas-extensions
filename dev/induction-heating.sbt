<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="3">
  <Definitions>
    <AttDef Type="induction-heating" BaseType="" Version="0">
      <ItemDefinitions>
        <!-- EM Source info (frequency vs. time)-->
        <Group Name="source" Label="Source" Extensible="True" NumberOfRequiredGroups="0" Version="0">
          <ItemDefinitions>
            <Double Name="time" Label="Time" Version="0"></Double>
            <Int Name="frequency" Label="Frequency" Version="0">
              <DefaultValue>1000</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
              </RangeInfo>
            </Int>
          </ItemDefinitions>
        </Group>
        <!-- Coils-->
        <Group Name="coils" Label="Coils" Extensible="True" NumberOfRequiredGroups="0" Version="0">
          <ItemDefinitions>
            <Int Name="nturns" Label="NTurns" Version="0">
              <BriefDescription>Number of turns of the coil</BriefDescription>
              <DefaultValue>1</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">1</Min>
              </RangeInfo>
            </Int>
            <Double Name="radius" Label="Radius" Version="0">
              <DefaultValue>1.0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0.0</Min>
              </RangeInfo>
            </Double>
            <Double Name="length" Label="Length" Version="0">
              <BriefDescription>Length of the coil</BriefDescription>
              <DefaultValue>1.0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0.0</Min>
              </RangeInfo>
            </Double>
            <Double Name="center" Label="Center" NumberOfRequiredValues="3" Version="0">
              <BriefDescription>The position of the center of the coil</BriefDescription>
              <DefaultValue>0.0</DefaultValue>
            </Double>
            <Double Name="current" Label="Current" Extensible="true" NumberOfRequiredValues="1">
              <BriefDescription>Electrical current</BriefDescription>
              <DefaultValue>0.0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0.0</Min>
              </RangeInfo>
            </Double>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
  <Views>
    <View Type="smtkTruchasCoilsView" Title="Induction Heating" TopLevel="true" FilterByAdvanceLevel="false" FilterByCategory="false">
      <InstancedAttributes>
        <Att Name="induction-heating" Type="induction-heating"></Att>
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeResource>
